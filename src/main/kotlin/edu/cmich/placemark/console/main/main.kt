package edu.cmich.placemark.console.main

import edu.cmich.placemark.console.controllers.PlacemarkController

fun main(args: Array<String>) {
    PlacemarkController().start()
}


